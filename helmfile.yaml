environments:
  develop:
    values:
      - domain: develop.kubernetes-platform-demo.express42.io
  production:
    values:
      - domain: kubernetes-platform-demo.express42.io

repositories:
- name: stable
  url: https://kubernetes-charts.storage.googleapis.com
- name: incubator
  url: https://kubernetes-charts-incubator.storage.googleapis.com/
- name: express42 
  url: https://raw.githubusercontent.com/express42/helm-charts/master
- name: istio.io
  url: https://storage.googleapis.com/istio-release/releases/1.4.2/charts/
- name: loki
  url: https://grafana.github.io/loki/charts
- name: reactiveops-stable
  url: https://charts.reactiveops.com/stable 
- name: fluxcd
  url: https://charts.fluxcd.io
- name: flagger 
  url: https://flagger.app
- name: jetstack
  url: https://charts.jetstack.io

templates:
  template: &template
    missingFileHandler: Info  
    atomic: true
    recreatePods: true
    wait: true
    values:
      - values/{{`{{ .Release.Name }}`}}.yaml.gotmpl

releases:
- name: namespaces
  chart: incubator/raw
  values:
  - resources:
    - apiVersion: v1
      kind: Namespace
      metadata:
        name: observability
      spec:
{{ if eq .Environment.Name "production" }}
    - apiVersion: v1
      kind: Namespace
      metadata:
        name: flux
      spec:
{{ end }}
    - apiVersion: v1
      kind: Namespace
      metadata:
        name: istio-system
      spec:

{{ if eq .Environment.Name "production" }}
- name: flux
  chart: fluxcd/flux
  version: 1.0.0
  namespace: flux
  <<: *template  
  needs:
    - namespaces
    - gcp-regional-sc

- name: helm-operator
  chart: fluxcd/helm-operator
  version: 0.4.0
  namespace: flux
  <<: *template  
  hooks:
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/flux-helm-release-crd.yaml"]
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/prometheus-operator

- name: flagger
  chart: flagger/flagger
  version: 0.21.0
  namespace: istio-system
  <<: *template  
  hooks:
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/weaveworks/flagger/master/artifacts/flagger/crd.yaml"]
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/prometheus-operator
{{ end }}

- name: gcp-regional-sc
  chart: express42/gcp-regional-sc
  version: 0.0.2
  <<: *template

- name: istio-init
  chart: istio.io/istio-init
  version: 1.4.2
  namespace: istio-system
  <<: *template
  hooks:
    - events: ["postsync"]
      command: "kubectl"
      args: ["--namespace", "istio-system", "wait", "--for=condition=complete", "job", "--all"]
  needs:
    - namespaces
    - gcp-regional-sc

- name: istio
  chart: istio.io/istio
  version: 1.4.2
  namespace: istio-system
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - istio-system/istio-init

- name: prometheus-operator
  chart: stable/prometheus-operator
  version: 8.3.2
  namespace: observability
  <<: *template
  hooks:
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/helm/charts/master/stable/prometheus-operator/crds/crd-alertmanager.yaml"]
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/helm/charts/master/stable/prometheus-operator/crds/crd-podmonitor.yaml"]
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/helm/charts/master/stable/prometheus-operator/crds/crd-prometheus.yaml"]
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/helm/charts/master/stable/prometheus-operator/crds/crd-prometheusrules.yaml"]
    - events: ["presync"]
      command: "kubectl"
      args: ["apply", "--validate=false", "--filename=https://raw.githubusercontent.com/helm/charts/master/stable/prometheus-operator/crds/crd-servicemonitor.yaml"]
  needs:
    - namespaces
    - gcp-regional-sc

- name: jaeger-operator
  chart: stable/jaeger-operator
  version: 2.12.1
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc

- name: jaeger
  chart: charts/jaeger
  version: 0.1.0
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/jaeger-operator

- name: loki
  chart: loki/loki-stack
  version: 0.22.0
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/prometheus-operator

- name: prometheus-telegram-bot
  chart: express42/prometheus-telegram-bot
  version: 0.1.3
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/prometheus-operator

- name: sonarqube
  chart: stable/sonarqube
  version: 3.2.7
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/prometheus-operator

- name: istio-gateways
  chart: charts/istio-gateways
  version: 0.1.0
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - istio-system/istio
    - observability/prometheus-operator
    - observability/sonarqube
    - observability/jaeger

- name: prometheus-rules
  chart: charts/prometheus-rules
  version: 0.1.0
  namespace: observability
  <<: *template
  needs:
    - namespaces
    - gcp-regional-sc
    - observability/prometheus-operator

